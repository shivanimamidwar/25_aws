#region
variable "region"{
    default = "us-east-2"
 }

 #Athena
variable "database_name"{
#  default = "example_database"
}

#DB
variable "table_name" {
  type = string
}

variable "hash_key_name" {
  type = string
}

variable "hash_key_type" {
  type = string
}

variable "sort_key_name" {
  type = string
}

variable "sort_key_type" {
  type = string
}

variable "read_capacity" {
  type = number
}

variable "write_capacity" {
  type = number
}


#ec2
variable "ec2_name"{
   #  default = "ec2_ezest"
}
variable "ec2_instance_type"{
   #  default = "t2.micro"
}
 
variable "ami"{
   #  default = "ami-05bfbece1ed5beb54"
}

variable "ec2_count"{
   #  default = "2"
}

variable "key_name"{
   #  default = "lantana"
}

variable "subnet_id"{
   #  default = "subnet-58565912"
}

variable "volume_size"{
   # default = 20
}

 variable "volume_type"{
   # default = "gp2"
}

variable "size"{
   # default = "gp2"
}

variable "sc_group_name"{}



#s3

variable "bucket_name"{  
   #  default     = "shivanimamidwar"  
}

variable "acl_enabled" {
  type        = bool
#   default     = true
}

variable "encryption_key_type" {
  type    = string
   # default = "aws:kms" #AES256
}

  variable "versioning_enabled" {
  type        = bool
  default     = true
}


#sagemaker
variable "sagemaker_instance_name"{
   # default = "mynotebook"
}

variable "sagemaker_instance_type"{
   #  default = "ml.t2.medium"
}

#lambda

variable "lambda_function_name"{
   # default = "shivanimamidwar"
}

#RDS
variable "identifier" {
   #  default = "identifier"
}

variable "instance_class" {
   #  default = "db.t3.micro"
}

variable "allocated_storage" {
   #  default = "5"
}

variable "engine" {
   #default = "mysql"
}

variable "engine_version" {
   #  default = "5.7"
}

variable "username" {
   #default = "sdcbhsjvcs"
}

variable "password" {
   #default = "shivanimamidwar"
}

variable "secret_name" {
#default = "rds_credentials"
}

#Load_balncer
variable "load_balancer_type" {
# default = "network" "application" "gateway"
}



#SQS
variable "visibility_timeout_seconds" {
  type = number
  default = 30
}

variable "message_retention_seconds" {
  type = number
  default = 345600
}

variable "queue_name" {
  type = string
  default = "standard"
}

variable "max_message_size_bytes" {
  type = number
  default = 262144
}

variable "delivery_delay_seconds" {
      default = 30
} 

variable "receive_wait_time_seconds" {
      default = 10
} 


#kinesis
variable "stream_name" {
  type = string
}

variable "shard_count" {
  type    = number
#   default = 1
}

#SNS
variable "sns_topic_name" {
  type = string
  default = "example-sns-topic"
}

variable "display_name" {
  type = string
  default = "Example SNS Topic"
}


#glacier
variable "glacier_vault_name"{}

variable "sns_topic"{}

#vpc
variable "subnet_count"{}

variable "vpc_cidr" {
#   default="172.31.0.0/16"
}

variable "vpc_name" {
#   default="172.31.0.0/16"
}

variable "ig_name" {
#   default="172.31.0.0/16"
}

variable "nat_gateway_name" {
#   default="172.31.0.0/16"
}










 


 



