#Athena
 database_name ="example_database"

#EC2
ec2_name = "ec2_ezest"
ec2_instance_type = "t2.micro"
ami = "ami-05bfbece1ed5beb54"
ec2_count = 2
key_name = "lantana"
subnet_id = "subnet-58565912"
volume_type = "gp2"
volume_size = 22
size = 23
sc_group_name = "ezest"

#RDS
identifier             = "shivani"
instance_class         = "db.t3.micro"
allocated_storage      = 5   
engine                 = "mysql"
engine_version         = "5.7"
username               = "shivanimamidwar"
password               = "mamidwarshivani"
secret_name            = "rds_credentials"

#s3
bucket_name = "bucketforezest"
encryption_key_type = "aws:kms" #(SSE-KMS)   #AES256 -(SSE-S3)
acl_enabled = true
versioning_enabled = true




#sagemaker_shivani
sagemaker_instance_name = "notebook"
sagemaker_instance_type = "ml.t2.medium" #ml.t2.xlarge

#lambda
lambda_function_name = "mamidwar"

#load_balancer
load_balancer_type = "application"


#kinesis
stream_name = "standard"
shard_count = "1"

#SNS
topic_name = "example-sns-topic"
display_name = "Example SNS Topic"

#Dynamo
table_name    = "Example_table"
hash_key_name = "UserId"
hash_key_type = "S"
sort_key_name = "GameTitle"
sort_key_type = "S"
read_capacity = 50
write_capacity = 50

#glacier
glacier_vault_name = "ezest"
sns_topic = "example"


#SQS
visibility_timeout_seconds= 20
message_retention_seconds = 345600
queue_name                = "message_queue"
max_message_size_bytes    = 262144
delivery_delay_seconds    = 30
receive_wait_time_seconds = 10

#VPC
subnet_count = 1
vpc_cidr = "172.31.0.0/16"
vpc_name = "vpc_for_ezest"
ig_name  = "ig_for_ezest"
nat_gateway_name = "nat_ezest"




