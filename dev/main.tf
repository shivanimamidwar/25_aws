module "ec2" {
  source = "../modules/ec2"
  ec2_name = var.ec2_name
  ami = var.ami
  ec2_instance_type = var.ec2_instance_type
  key_name = var.key_name
  ec2_count     = var.ec2_count
  subnet_id = var.subnet_id
  volume_type = var.volume_type
  volume_size = var.volume_size
  size = var.size
  sc_group_name  = var.sc_group_name
}

module "rds" {
  source = "../modules/rds"
  identifier             = var.identifier  
  instance_class         = var.instance_class 
  allocated_storage      = var.allocated_storage   
  engine                 = var.engine
  engine_version         = var.engine_version 
  username               = var.username  
  password               = var.password
}

module "s3" {
  source = "../modules/s3"
  bucket_name = var.bucket_name
  acl_enabled = var.acl_enabled
  versioning_enabled = var.versioning_enabled
  encryption_key_type = var.encryption_key_type

}

module "sagemaker" {
  source = "../modules/sagemaker"
  sagemaker_instance_name = var.sagemaker_instance_name
  sagemaker_instance_type = var.sagemaker_instance_type
  
}

module "load_balancer" {
  source = "../modules/load_balancer"
  load_balancer_type = var.load_balancer_type
}

module "sqs" {
  source = "../modules/sqs"
}

module "sns" {
  source = "../modules/sns"  
}


module "dynamodb" {
  source = "../modules/dynamodb"
  table_name     = var.table_name
  hash_key_name  = var.hash_key_name
  hash_key_type  = var.hash_key_type
  sort_key_name  = var.sort_key_name
  sort_key_type  = var.sort_key_type
  read_capacity  = var.write_capacity
  write_capacity = var.write_capacity 
}

module "glacier"{
  source = "../modules/glacier"
  glacier_vault_name = var.glacier_vault_name
  sns_topic = var.sns_topic
  
}

module "athena" {
  source = "../modules/athena"
  database_name = var.database_name
}

module "kinesis" {
  source = "../modules/kinesis"
  stream_name = var.stream_name
  shard_count = var.shard_count
  
}

module "lambda" {
  source = "../modules/lambda"
  lambda_function_name = var.lambda_function_name
}
module "vpc" {
  source = "../modules/vpc"
  subnet_count = var.subnet_count
  vpc_cidr = var.vpc_cidr
  vpc_name = var.vpc_name
  ig_name  = var.ig_name
  nat_gateway_name = var.nat_gateway_name
}