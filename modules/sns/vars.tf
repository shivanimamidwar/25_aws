variable "sns_topic_name" {
  type = string
  default = "example-sns-topic"
}

variable "display_name" {
  type = string
  default = "Example SNS Topic"
}



