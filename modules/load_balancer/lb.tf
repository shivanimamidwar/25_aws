provider "aws" {
  region = "us-east-2"
}

locals {
  load_balancer_type = var.load_balancer_type
}


resource "aws_vpc" "shivani" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "example-vpc"
  }
}

resource "aws_internet_gateway" "example_igw" {
  vpc_id = aws_vpc.shivani.id
}


resource "aws_subnet" "examplea_az1" {
  vpc_id     = aws_vpc.shivani.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-2a"

  tags = {
    Name = "example-subnet-az1"
  }
}

resource "aws_subnet" "examplea_az2" {
  vpc_id     = aws_vpc.shivani.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "us-east-2b"

  tags = {
    Name = "example-subnet-az2"
  }
}



resource "aws_lb" "mamidwar" {
  name            = "example-lb"
  internal        = false
  load_balancer_type = var.load_balancer_type

  security_groups = []
  subnets         = [aws_subnet.examplea_az1.id, aws_subnet.examplea_az2.id] 
}

resource "aws_lb_target_group" "example" {
  name = "example-tg"

  port     = 80
  protocol = "HTTP"

  vpc_id = aws_vpc.shivani.id
}



# resource "aws_elb_attachment" "example" {
#   elb      = aws_elb.example.id
#   instance = aws_instance.example.id
# }
