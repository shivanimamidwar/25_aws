
resource "aws_sns_topic" "aws_sns_topic" {
  name = var.sns_topic
}

resource "aws_glacier_vault" "my_archive" {
  name = var.glacier_vault_name

  notification {
    sns_topic = aws_sns_topic.aws_sns_topic.arn
    events    = ["ArchiveRetrievalCompleted", "InventoryRetrievalCompleted"]
  }
}