variable "queue_name" {
  default = "example_queue"
}

variable "visibility_timeout_seconds" {
  type = number
  default = 30
}

variable "message_retention_seconds" {
  type = number
  default = 345600
}


variable "max_message_size_bytes" {
  type = number
  default = 262144
}

variable "delivery_delay_seconds" {
      default = 30
} 

variable "receive_wait_time_seconds" {
      default = 10
} 



