
# locals {
#  queue_type = var.queue_type
# }
resource "aws_sqs_queue" "example_queue" {
  name                      = var.queue_name
  visibility_timeout_seconds= var.visibility_timeout_seconds
  message_retention_seconds = var.message_retention_seconds
  delay_seconds             = var.delivery_delay_seconds
  max_message_size          = var.max_message_size_bytes
  receive_wait_time_seconds = var.receive_wait_time_seconds 

  tags = {
    Terraform = "true"
  }
}
