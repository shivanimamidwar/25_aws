
resource "aws_glue_catalog_database" "example_database" {
  name = var.database_name
}

output "database_name" {
  value = "${aws_glue_catalog_database.example_database.name}"
}
