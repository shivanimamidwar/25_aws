data "archive_file" "lambda" {
  type        = "zip"
  source_file  = "${"../modules/lambda/lambda.py"}"
  output_path  = "${"../modules/lambda/lambda.zip"}"
}


resource "aws_lambda_function" "lambda_shivani_function" {
  
  function_name = var.lambda_function_name
  filename = "${"../modules/lambda/lambda.zip"}"
  role = aws_iam_role.lambda_role_21.arn
  handler = "exports.test"
  runtime = "python3.9"
    
}

resource "aws_iam_role" "lambda_role_21" {
  name = "lambda_role21"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy" "lambda_policy" {
  name = "lambda_policy"
  role = aws_iam_role.lambda_role_21.id
  
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
    {
      "Sid": "Stmt1672904312992",
      "Action": "sagemaker:*",
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
  })
}





