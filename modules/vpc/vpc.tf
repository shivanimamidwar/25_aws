resource "aws_vpc" "main" {
  cidr_block = "${var.vpc_cidr}"
  instance_tenancy = "default"

  tags = {
    Name = var.vpc_name
     }
}

#Creating 3 Subnets in VPC
resource "aws_subnet" "main" {
count = var.subnet_count
vpc_id = "${aws_vpc.main.id}"
cidr_block = "${cidrsubnet(var.vpc_cidr,8,count.index)}"
map_public_ip_on_launch = "true"
tags = {
  Name="Subnet-${count.index + 0}"
}
}

# Creating Internet Gateway in AWS VPC
resource "aws_internet_gateway" "IG" {
  vpc_id ="${aws_vpc.main.id}"
  

  tags = {
    Name = var.ig_name
  }
}


#Creating EIP for NAT gateway
resource "aws_eip" "example" {
  vpc = true

tags = {
    Name = "elastic_IP"
  }
}

#Creating NAT gateway 
resource "aws_nat_gateway" "example" {
  count = var.subnet_count
  allocation_id = aws_eip.example.id
  subnet_id     = aws_subnet.main[count.index].id

  tags = {
    Name = var.nat_gateway_name
  }
}






