
resource "aws_security_group" "ezest" {
  name        = var.sc_group_name
  description = "Allow HTTP and HTTPS access"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "SecurityGroup"
  }
}

resource "aws_instance" "instance" {
  ami   = var.ami
  instance_type = var.ec2_instance_type
  count     = var.ec2_count
  key_name = var.key_name
  subnet_id = var.subnet_id
  security_groups = [aws_security_group.ezest.id]
  tags = {
    Name = var.ec2_name
  }

  root_block_device {
    volume_type = var.volume_type
    volume_size = var.volume_size
  }

}

resource "aws_ebs_volume" "volume" {
  count     = var.ec2_count
  availability_zone = aws_instance.instance[count.index].availability_zone


// Size IN GiB
    size = var.size

    tags = {

        Name = "terraformTesting"
    }    
}

resource "aws_volume_attachment" "ebsAttach" {
    count     = var.ec2_count
    device_name = "/dev/sdh"
    volume_id = aws_ebs_volume.volume[count.index].id
    instance_id = aws_instance.instance[count.index].id

}



