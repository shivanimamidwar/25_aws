resource "aws_s3_bucket" "example" {
  bucket = var.bucket_name

  acl = var.acl_enabled ? "private" : "public-read"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = var.encryption_key_type
      }
    }  
}

}

resource "aws_s3_bucket_versioning" "versioning_example" {
  bucket = aws_s3_bucket.example.id
  versioning_configuration {
    status = var.versioning_enabled ? "Enabled" : "Disabled"
  }
}



  
  


