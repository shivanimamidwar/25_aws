resource "aws_db_instance" "shivaniii" {
  identifier             = var.identifier  
  instance_class         = var.instance_class 
  allocated_storage      = var.allocated_storage   
  engine                 = var.engine
  engine_version         = var.engine_version 
  username               = var.username  
  password               = var.password
  publicly_accessible    = true
  skip_final_snapshot    = true
}

resource "aws_secretsmanager_secret" "rds_secrets" {
  name = var.secret_name
}

resource "aws_secretsmanager_secret_version" "rds_secrets" {
  secret_id     = aws_secretsmanager_secret.rds_secrets.id
  secret_string = jsonencode({username = var.username,password = var.password})
}