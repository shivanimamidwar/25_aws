variable "stream_name" {
  type = string
}

variable "shard_count" {
  type    = number
  # default = 1
}


variable "read_capacity_units" {
  type = number
  default = 1
}

variable "write_capacity_units" {
  type = number
  default = 1
}