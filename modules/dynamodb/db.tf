
resource "aws_dynamodb_table" "example" {
  name           = var.table_name
  hash_key       = var.hash_key_name
  range_key      = var.sort_key_name
  read_capacity  = var.write_capacity
  write_capacity = var.write_capacity


  attribute {
    name = "${var.hash_key_name}"
    type = "${var.hash_key_type}"
  }

  attribute {
     name = "${var.sort_key_name}"
     type = "${var.sort_key_type}"
  }

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

