variable "table_name" {
  type = string
}

variable "hash_key_name" {
  type = string
}

variable "hash_key_type" {
  type = string
}

variable "sort_key_name" {
  type = string
}

variable "sort_key_type" {
  type = string
}

variable "read_capacity" {
  type = number
}

variable "write_capacity" {
  type = number
}
